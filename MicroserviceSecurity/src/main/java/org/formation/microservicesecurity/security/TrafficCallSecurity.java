package org.formation.microservicesecurity.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.NimbusJwtDecoder;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.List;

@Configuration
@EnableWebSecurity
@Profile(value = {"prod", "dev"})
public class TrafficCallSecurity {
    @Value("${spring.security.oauth2.resourceserver.jwt.jwk-set-uri}")
    private String jwkSetUri;

    @Bean
    @Profile("dev")
    public SecurityFilterChain trafficFilterChainDev(final HttpSecurity httpDev) throws Exception {
        return httpDev
                .sessionManagement(s -> s.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
                .csrf(AbstractHttpConfigurer::disable)
                .cors(corsConfig -> corsConfig.configurationSource(corsConfigurationSourceDev()))
                .authorizeHttpRequests(requete -> requete.anyRequest().anonymous())
                .build();
    }

    @Bean
    @Profile("prod")
    public SecurityFilterChain trafficFilterChainProd(final HttpSecurity httprod) throws Exception {
        httprod.authorizeHttpRequests(a -> a.anyRequest().fullyAuthenticated()
                ).sessionManagement(s -> s.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
                .cors(c -> c.configurationSource(this.corsConfigurationSourceProd()))
                .csrf(AbstractHttpConfigurer::disable)
                .oauth2ResourceServer(r -> r.jwt((jwt) -> jwt.decoder(this.jwtDecoder())));
        return httprod.build();
    }

    @Bean
    @Profile("dev")
    public CorsConfigurationSource corsConfigurationSourceDev() {
        CorsConfiguration configurationDev = new CorsConfiguration();
        configurationDev.setAllowedOrigins(List.of("*")); // Quelque soit l'origine
        configurationDev.setAllowedMethods(List.of("*")); // Quelle que soit la méthode (GET, POST, PUT, PATCH, etc.)
        configurationDev.setAllowedHeaders(List.of("*")); // Quel que soit le type de paylod (JSON, HTML, etc.) meme sans autorisation !
        UrlBasedCorsConfigurationSource sourceDev = new UrlBasedCorsConfigurationSource();
        sourceDev.registerCorsConfiguration("/**", configurationDev);
        return sourceDev;
    }

    @Bean
    @Profile("prod")
    public CorsConfigurationSource corsConfigurationSourceProd() {
        CorsConfiguration corsConfigurationProd = new CorsConfiguration();
        corsConfigurationProd.setAllowedOrigins(List.of("*")); // Définir ici votre domaine. Pour cet exemple on utilise * qui n'est pas recommandé
        corsConfigurationProd.setAllowedMethods(List.of("GET","POST","PUT","DELETE")); // Restreindre aux requêtes GET, POST, PUT, DELETE
        corsConfigurationProd.setAllowedHeaders(List.of("Content-Type", "Authorization"));
        UrlBasedCorsConfigurationSource sourceProd = new UrlBasedCorsConfigurationSource();
        sourceProd.registerCorsConfiguration("/**", corsConfigurationProd);
        return sourceProd;
    }

    public JwtAuthenticationConverter jwtAuthenticationConverter() {
        JwtAuthenticationConverter converter = new JwtAuthenticationConverter();
        converter.setJwtGrantedAuthoritiesConverter(new AutorityConverter());
        return converter;
    }

    @Bean
    public JwtDecoder jwtDecoder() {
        return NimbusJwtDecoder.withJwkSetUri(jwkSetUri).build();
    }
}
