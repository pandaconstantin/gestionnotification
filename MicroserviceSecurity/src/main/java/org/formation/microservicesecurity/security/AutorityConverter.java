package org.formation.microservicesecurity.security;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.jwt.Jwt;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class AutorityConverter implements Converter<Jwt, Collection<GrantedAuthority>> {
    /**
     * @param source
     * @return
     */
    @Override
    public Collection<GrantedAuthority> convert(Jwt source) {
        Map<String, Object> claims = source.getClaimAsMap("realm_access"); // Rechercher dans l'objet JSON du jeton la clé "realm_acces"
        if (claims != null) {
            List<String> listeRoles =  (List<String>) claims.get("roles");
            if (!listeRoles.isEmpty()) {
                return listeRoles.stream().map(role -> new SimpleGrantedAuthority("ROLE_" + role))
                        .collect(Collectors.toList());
            }
        }
        return List.of();
    }
}