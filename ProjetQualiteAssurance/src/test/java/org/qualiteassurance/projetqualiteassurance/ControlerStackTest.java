package org.qualiteassurance.projetqualiteassurance;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.qualiteassurance.projetqualiteassurance.controleurs.GestionNotificationControleur;
import org.qualiteassurance.projetqualiteassurance.database.entites.SMSNotification;
import org.qualiteassurance.projetqualiteassurance.services.GestionNotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import java.time.LocalDate;
import java.util.List;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Classe pour tester la couche contrôleur.
 *
 * @author Constantin Drabo
 * @version 1.0
 * @since 27-06-2024
 */
@WebMvcTest(GestionNotificationControleur.class)
public class ControlerStackTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    GestionNotificationService gestionNotificationService;

    @Test
    public void when_get_then_listeSMS() throws Exception {
        //Arrange
        SMSNotification smsNotification = new SMSNotification();
        smsNotification.setId(1L);
        smsNotification.setNomComplet("Constantin Drabo");
        smsNotification.setNumeroTelephone("58232615");
        smsNotification.setDateEnvoi(LocalDate.now());

        SMSNotification smsNotification2 = new SMSNotification();
        smsNotification2.setId(2L);
        smsNotification2.setNomComplet("Emmanuel Drabo");
        smsNotification2.setNumeroTelephone("58232616");
        smsNotification2.setDateEnvoi(LocalDate.now());

        //Act
        Mockito.when(this.gestionNotificationService.listerSMSNotification()).thenReturn(List.of(smsNotification, smsNotification2));
        mockMvc.perform(get("/sms-notification"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[*].numeroTelephone").value(containsInAnyOrder("58232615","58232616")));
    }

    @Test
    public void when_email_then_enregistrer() throws Exception {
        String emailPayload = "{\"id\":null,\"nomComplet\":\"Constantin Drabo\",\"emailAdresse\":\"panda.constantin@gmail.com\"}";

        mockMvc.perform(post("/email-notification").contentType(MediaType.APPLICATION_JSON).content(emailPayload))
                .andExpect(status().isCreated())
                .andDo(print());
    }


    @Test
    public void when_id_then_liste_sms() throws Exception {
        //Arrange
        SMSNotification smsNotification = new SMSNotification();
        smsNotification.setId(1L);
        smsNotification.setNomComplet("Constantin Drabo");
        smsNotification.setNumeroTelephone("58232615");
        smsNotification.setDateEnvoi(LocalDate.now());

        SMSNotification smsNotification2 = new SMSNotification();
        smsNotification2.setId(2L);
        smsNotification2.setNomComplet("Emmanuel Drabo");
        smsNotification2.setNumeroTelephone("58232616");
        smsNotification2.setDateEnvoi(LocalDate.now());

        Mockito.when(gestionNotificationService.findSMSNotificationById(anyLong())).thenReturn(smsNotification);

        mockMvc.perform(get("/sms-notification/2"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.numeroTelephone").value(smsNotification.getNumeroTelephone()))
                .andDo(print());
    }

}
