package org.qualiteassurance.projetqualiteassurance;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.qualiteassurance.projetqualiteassurance.database.entites.SMSNotification;
import org.qualiteassurance.projetqualiteassurance.database.repository.SMSNotificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.util.Assert;
import java.time.LocalDate;
import java.util.List;

/**
 * Classe de test de la couche Model de notre application.
 *
 * @author Constantin Drabo
 * @version 1.0
 * @since 27-06-2024
 */
@DataJpaTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class ModelStackQATest {

    @Autowired
    SMSNotificationRepository smsNotificationRepository;

    @Test
    public void when_payload_then_enregistrerSMS() {
        // Arrange - Recherche des informations de base
        SMSNotification smsNotification = new SMSNotification();
        smsNotification.setNomComplet("Serge");
        smsNotification.setNumeroTelephone("70580125");
        smsNotification.setDateEnvoi(LocalDate.now());
        //Act - Action à effectuer
        smsNotification = smsNotificationRepository.save(smsNotification);
        List<SMSNotification> listeSMS = smsNotificationRepository.findAll();
        //System.out.println("Le nombre d'element");
        //Assert - Définir les assertions
        Assert.notNull(smsNotification.getId(), "Le SMS n'est pas enregistré");
        //Assert.isTrue(listeSMS.size() == 2, "Le nombre d'éléments n'est pas atteint");
        Assertions.assertEquals(1, listeSMS.size());
    }

}
