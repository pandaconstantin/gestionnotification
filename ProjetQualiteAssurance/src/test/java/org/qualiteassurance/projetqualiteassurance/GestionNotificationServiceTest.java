package org.qualiteassurance.projetqualiteassurance;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.qualiteassurance.projetqualiteassurance.database.entites.EmailNotification;
import org.qualiteassurance.projetqualiteassurance.database.entites.Notification;
import org.qualiteassurance.projetqualiteassurance.database.repository.EmailNotificationRepository;
import org.qualiteassurance.projetqualiteassurance.database.repository.NotificationRepository;
import org.qualiteassurance.projetqualiteassurance.services.GestionNotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

public class GestionNotificationServiceTest extends ApplicationTest {

    @Autowired private EmailNotificationRepository emailNotificationRepository;
    @Autowired private GestionNotificationService gestionNotificationService;
    @Autowired private NotificationRepository notificationRepository;


    @Test
    public void when_payload_then_modify_email() {
         // Arrange
        List<Notification> listeNotification = notificationRepository.findAll();
        EmailNotification payload  = emailNotificationRepository
                .findById(2L).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Email introuvable"));
        payload.setEmailAdresse("elonmusk.twitter.com");
        EmailNotification emailNotificationModified  = gestionNotificationService.updateEmailNotification(payload, 2L);
        Assertions.assertEquals("elonmusk.twitter.com", emailNotificationModified.getEmailAdresse());
        Assertions.assertEquals(3, listeNotification.size());
    }

    @Test
    public void when_get_on_email_then_200() throws Exception {
        mockMvc.perform(get("/email-notification"))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void when_get_on_email_then_404() throws Exception {
        mockMvc.perform(get("/email-notification/"))
                .andExpect(status().isNotFound());
    }
}



