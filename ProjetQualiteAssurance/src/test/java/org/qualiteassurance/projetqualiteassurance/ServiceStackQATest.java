package org.qualiteassurance.projetqualiteassurance;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.qualiteassurance.projetqualiteassurance.database.entites.SMSNotification;
import org.qualiteassurance.projetqualiteassurance.database.repository.SMSNotificationRepository;
import org.qualiteassurance.projetqualiteassurance.services.GestionNotificationService;
import java.time.LocalDate;
import java.util.List;


/**
 * Classe pour tester la couche service
 * @author Constantin Drabo
 * @version 1.0
 * @since 27-06-2024
 */
@ExtendWith(MockitoExtension.class)
public class ServiceStackQATest {

    @Mock
    SMSNotificationRepository smsNotificationRepository;

    @InjectMocks
    private GestionNotificationService gestionNotificationService;

    @Test
    public void when_sms_payload_then_save() {
        // Arrange
        SMSNotification smsNotification = new SMSNotification();
        smsNotification.setId(1L);
        smsNotification.setNomComplet("Constantin Drabo");
        smsNotification.setNumeroTelephone("58232615");
        smsNotification.setDateEnvoi(LocalDate.now());

        SMSNotification smsNotification2 = new SMSNotification();
        smsNotification.setId(2L);
        smsNotification.setNomComplet("Emmanuel Drabo");
        smsNotification.setNumeroTelephone("58232616");
        smsNotification.setDateEnvoi(LocalDate.now());

        // Act
        Mockito.when(this.smsNotificationRepository.findAll()).thenReturn(List.of(smsNotification, smsNotification2));
        List<SMSNotification> listeNotifications = gestionNotificationService.listerSMSNotification();
        // Assert
        Assertions.assertEquals(2, listeNotifications.size());

    }

}
