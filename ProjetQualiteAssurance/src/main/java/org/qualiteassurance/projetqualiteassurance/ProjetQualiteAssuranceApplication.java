package org.qualiteassurance.projetqualiteassurance;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetQualiteAssuranceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProjetQualiteAssuranceApplication.class, args);
    }

}
