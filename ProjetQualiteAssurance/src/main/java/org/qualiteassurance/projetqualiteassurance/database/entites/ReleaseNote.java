package org.qualiteassurance.projetqualiteassurance.database.entites;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class ReleaseNote {
    @Id
    private String releaseNoteId;
    private String releaseNote;
    private LocalDate releaseDate;
}
