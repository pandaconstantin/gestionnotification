package org.qualiteassurance.projetqualiteassurance.database.repository;

import org.qualiteassurance.projetqualiteassurance.database.entites.EmailNotification;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmailNotificationRepository extends JpaRepository<EmailNotification, Long> {
}