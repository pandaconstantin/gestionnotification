package org.qualiteassurance.projetqualiteassurance.database.entites;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.context.annotation.Scope;

@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Scope("prototype")
public class SMSNotification extends Notification {
  @Column(name = "numero_telephone")
  private String numeroTelephone;
}