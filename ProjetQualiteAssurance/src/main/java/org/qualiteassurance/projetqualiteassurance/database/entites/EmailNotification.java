package org.qualiteassurance.projetqualiteassurance.database.entites;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class EmailNotification extends Notification {
  @Column(name = " ")
  private String emailAdresse;
}