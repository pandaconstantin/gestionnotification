package org.qualiteassurance.projetqualiteassurance.database.repository;

import org.qualiteassurance.projetqualiteassurance.database.entites.ReleaseNote;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReleaseNoteRepository extends JpaRepository<ReleaseNote, String> {
}