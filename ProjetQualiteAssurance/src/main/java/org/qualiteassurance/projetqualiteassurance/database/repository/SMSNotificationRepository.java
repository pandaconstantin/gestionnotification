package org.qualiteassurance.projetqualiteassurance.database.repository;

import org.qualiteassurance.projetqualiteassurance.database.entites.SMSNotification;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SMSNotificationRepository extends JpaRepository<SMSNotification, Long> {
}