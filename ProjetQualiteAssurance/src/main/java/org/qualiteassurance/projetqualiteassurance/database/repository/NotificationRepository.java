package org.qualiteassurance.projetqualiteassurance.database.repository;

import org.qualiteassurance.projetqualiteassurance.database.entites.Notification;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NotificationRepository extends JpaRepository<Notification, Long> {
}