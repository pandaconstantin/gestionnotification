package org.qualiteassurance.projetqualiteassurance.database.entites;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.context.annotation.Scope;

import java.time.LocalDate;

@Getter
@Setter
@Entity
@Table(name = "notification")
@NoArgsConstructor
@AllArgsConstructor
@Scope("prototype")
public abstract class Notification {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="notification_id_seqence_gen")
    @SequenceGenerator(name = "notification_id_seqence_gen", sequenceName = "notification_seq" , allocationSize = 1)
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(name = "nom_complet")
    private String nomComplet;
    @Column(name = "date_envoi")
    private LocalDate dateEnvoi;
}