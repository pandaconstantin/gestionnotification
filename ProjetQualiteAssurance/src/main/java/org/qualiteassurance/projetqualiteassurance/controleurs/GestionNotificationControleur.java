package org.qualiteassurance.projetqualiteassurance.controleurs;


import lombok.RequiredArgsConstructor;
import org.qualiteassurance.projetqualiteassurance.database.entites.EmailNotification;
import org.qualiteassurance.projetqualiteassurance.database.entites.SMSNotification;
import org.qualiteassurance.projetqualiteassurance.services.GestionNotificationService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RequiredArgsConstructor
@RestController
public class GestionNotificationControleur {

    private final GestionNotificationService services;

    @PostMapping("sms-notitification")
    public ResponseEntity<SMSNotification> createSMSNotification(
            @RequestBody SMSNotification sms) {
        return new ResponseEntity<>(services.enregistrerSMSNotification(sms), HttpStatus.CREATED);
    }

    @PostMapping("email-notification")
    public ResponseEntity<EmailNotification> createEmailNotification(@RequestBody EmailNotification email) {
        return new ResponseEntity<>(services.enregistrerEmailNotification(email), HttpStatus.CREATED);
    }

    @GetMapping("sms-notification")
    public ResponseEntity<List<SMSNotification>> getListeSMSNotification() {
        return new ResponseEntity<>(services.listerSMSNotification(), HttpStatus.OK);
    }

    @GetMapping("email-notification")
    public ResponseEntity<List<EmailNotification>> getListeEmailNotification() {
        return new ResponseEntity<>(services.listerEmailNotification(), HttpStatus.OK);
    }

    @GetMapping("sms-notification/{id}")
    public ResponseEntity<SMSNotification> getSMSNotificationById(@PathVariable Long id) {
        return new ResponseEntity<>(services.findSMSNotificationById(id),HttpStatus.OK);
    }

    @GetMapping("email-notification/{id}")
    public ResponseEntity<EmailNotification> getEmailNotificationById(Long id) {
        return new ResponseEntity<>(services.findEmailNotificationById(id),HttpStatus.OK);
    }
}
