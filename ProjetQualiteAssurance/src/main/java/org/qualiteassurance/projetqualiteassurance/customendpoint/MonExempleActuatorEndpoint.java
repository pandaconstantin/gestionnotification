package org.qualiteassurance.projetqualiteassurance.customendpoint;

import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.stereotype.Component;

@Component
@Endpoint(id = "exempleactuor")
public class MonExempleActuatorEndpoint {
    private String informationMicroservice;

    public MonExempleActuatorEndpoint() {
    }

    public String getInformationMicroservice() {
        return informationMicroservice;
    }

    public void setInformationMicroservice(String informationMicroservice) {
        this.informationMicroservice = informationMicroservice;
    }

    @ReadOperation
    public String getInformationMicroserviceName() {
        return "Information du microservice";
    }
}
