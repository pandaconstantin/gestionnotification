package org.qualiteassurance.projetqualiteassurance.services;

import lombok.RequiredArgsConstructor;
import org.qualiteassurance.projetqualiteassurance.database.entites.EmailNotification;
import org.qualiteassurance.projetqualiteassurance.database.entites.SMSNotification;
import org.qualiteassurance.projetqualiteassurance.database.repository.EmailNotificationRepository;
import org.qualiteassurance.projetqualiteassurance.database.repository.SMSNotificationRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class GestionNotificationService {

    private final SMSNotificationRepository smsNotificationRepository;
    private final EmailNotificationRepository emailNotificationRepository;

    /**
     * Enregistre un nouveau SMS
     * @param sms Wrapper SMS contenant les informations du SMS
     * @return SMSNotification
     */
    public SMSNotification enregistrerSMSNotification(SMSNotification sms) {
        System.out.println("Here");
        return smsNotificationRepository.save(sms);
    }

    /**
     * Enregistrer un nouvel email
     * @param email Une notification de type email
     * @return EmailNotification
     */
    public EmailNotification enregistrerEmailNotification(EmailNotification email) {
        return emailNotificationRepository.save(email);
    }

    /**
     * Retourne la liste des notifications de type SMS
     * @return List<Notification>
     */
    public List<SMSNotification> listerSMSNotification() {
        return smsNotificationRepository.findAll();
    }

    /**
     * Retourne la liste des notifications de type email
     * @return
     */
    public List<EmailNotification> listerEmailNotification() {
        return emailNotificationRepository.findAll();
    }

    /**
     * Recherche un SMS en fournissant l'identifiant de la notification.
     * @param id Identifiant de la notification
     * @return SMSNotification
     */
    public SMSNotification findSMSNotificationById(Long id) {
        Optional<SMSNotification> smsNotificationOptional = smsNotificationRepository.findById(id);
        if (smsNotificationOptional.isPresent()) {
            return smsNotificationOptional.get();
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "SMS introuvable");
        }
    }

    public EmailNotification findEmailNotificationById(Long id) {
        Optional<EmailNotification> emailNotificationOptional = emailNotificationRepository.findById(id);
        if(emailNotificationOptional.isPresent()) {
            return emailNotificationOptional.get();
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Email introuvable");
        }
    }

    /**
     * Méthode qui permet de modifier une notification de type email.
     * @param payload Wrapper qui contient les informations de modification
     * @param id Identifiant de la notification
     * @return EmailNotification
     */
    public EmailNotification updateEmailNotification(EmailNotification payload, Long id) {
        EmailNotification emailNotification = emailNotificationRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Email introuvable"));
        payload.setId(id);
        return emailNotificationRepository.save(payload);
    }

}
