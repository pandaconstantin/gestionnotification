package org.qualiteassurance.projetqualiteassurance.services;

import lombok.RequiredArgsConstructor;
import org.qualiteassurance.projetqualiteassurance.database.entites.ReleaseNote;
import org.qualiteassurance.projetqualiteassurance.database.repository.ReleaseNoteRepository;
import org.springframework.boot.actuate.endpoint.annotation.DeleteOperation;
import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.boot.actuate.endpoint.annotation.Selector;
import org.springframework.boot.actuate.endpoint.annotation.WriteOperation;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

/**
 * Classe qui fournit l'endpoint Actuator sur les releases.
 * @version 1.0
 * @since  28 juin 2024
 * @author Constantin Drabo
 */
@RequiredArgsConstructor
@Service
@Endpoint(id="releasenoteapplication")
public class ReleaseNoteEndpoint {
    private final ReleaseNoteRepository releaseNoteRepository;

    @ReadOperation
    public List<ReleaseNote> getReleaseNotes() {
        return releaseNoteRepository.findAll();
    }

    @WriteOperation
    public ReleaseNote addReleaseNote(String releaseNote) {
        ReleaseNote release = new ReleaseNote();
        release.setReleaseNote(releaseNote);
        release.setReleaseNoteId(UUID.randomUUID().toString());
        release.setReleaseDate(LocalDate.now());
        return releaseNoteRepository.save(release);
    }

    @DeleteOperation
    public ReleaseNote deleteReleaseNote(@Selector String releaseNoteId) {
        ReleaseNote release = releaseNoteRepository.findById(releaseNoteId)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "La release est introuvable"));
        releaseNoteRepository.delete(release);
        return release;
    }
}
