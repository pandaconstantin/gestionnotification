package org.gestionnotification.gestiondiscovery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestionDiscoveryApplication {

    public static void main(String[] args) {
        SpringApplication.run(GestionDiscoveryApplication.class, args);
    }

}
