package org.gestionportefeuille.gestionportefeuille.modele;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AchatDto {
    private String idAchat;
    private String idClient;
    private String nomClient;
    private String prenomClient;
    private String libelleProduit;
    private LocalDate dateAchat;
}
