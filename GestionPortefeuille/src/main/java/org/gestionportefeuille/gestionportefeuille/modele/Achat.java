package org.gestionportefeuille.gestionportefeuille.modele;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Document(indexName = "achat")
public class Achat {
    @Id
    private String idAchat;
    @Field(name = "idclient")
    private String idClient;
    @Field(name = "idproduit")
    private String idProduit;
    @Field(name = "dateachat", type = FieldType.Date, pattern = "dd.MM.uuuu")
    private LocalDate dateAchat;
}
