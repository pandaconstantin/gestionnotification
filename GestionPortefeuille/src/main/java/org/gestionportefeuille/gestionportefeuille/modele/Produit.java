package org.gestionportefeuille.gestionportefeuille.modele;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Document(indexName = "produit")
public class Produit {
    @Id
    private String idProduit;
    @Field(name = "libelle")
    private String libelle;
}
