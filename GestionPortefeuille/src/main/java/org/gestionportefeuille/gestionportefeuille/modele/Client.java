package org.gestionportefeuille.gestionportefeuille.modele;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.stereotype.Component;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Document(indexName = "client")
public class Client {
    @Id
    private String idClient;
    @Field(name = "nom")
    private String nom;
    @Field(name = "prenom")
    private String prenom;
    @Field(name = "numerotelephone")
    private String numeroTelephone;
    @Field(name = "emailadresse")
    private String emailAdresse;
}
