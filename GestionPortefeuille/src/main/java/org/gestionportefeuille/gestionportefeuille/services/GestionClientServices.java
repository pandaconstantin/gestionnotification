package org.gestionportefeuille.gestionportefeuille.services;

import lombok.RequiredArgsConstructor;
import org.gestionportefeuille.gestionportefeuille.api.GestionClientApiClient;
import org.gestionportefeuille.gestionportefeuille.modele.Achat;
import org.gestionportefeuille.gestionportefeuille.modele.AchatDto;
import org.gestionportefeuille.gestionportefeuille.modele.Client;
import org.gestionportefeuille.gestionportefeuille.modele.Produit;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class GestionClientServices {

    private final GestionClientApiClient gestionClientApiClient;

    /**
     * Enregistrer un client
     * @param client Wrapper contenant les informations du client
     * @return Client
     */
    public Client enregistrerClient(final Client client) {
        return gestionClientApiClient.createClient(client);
    }

    /**
     * Modifier un client.
     * @param payload Wrapper contenant les informations pour la modification
     * @param idClient Identifiant du client
     * @return Client.
     */
    public Client modifierClient(final Client payload, final String idClient) {
        return gestionClientApiClient.updateClient(payload, idClient);
    }

    /**
     * Retourne la liste des clients.
     * @return List<Client>
     */
    public List<Client> listeClient() {
        return gestionClientApiClient.getListClients();
    }

    /**
     * Retourne un client en fonction de son identifiant.
     * @param idClient Identifiant du client
     * @return Client
     */
    public Client listeUnClient(final String idClient) {
        return gestionClientApiClient.getClientFromId(idClient);
    }

    public Produit enregistrerProduit(final Produit produit) {
        return gestionClientApiClient.createProduit(produit);
    }

    public List<Produit> listeProduit() {
        return gestionClientApiClient.getListProduits();
    }

    public Achat enregistrerAchat(final Achat achat) {
        return gestionClientApiClient.createAchat(achat);
    }

    public List<AchatDto> listeAchats() {
        return gestionClientApiClient.getListAchats();
    }
}
