package org.gestionportefeuille.gestionportefeuille;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"org.formation.microservicesecurity.security","org.gestionportefeuille.gestionportefeuille"})
public class GestionPortefeuilleApplication {

    public static void main(String[] args) {
        SpringApplication.run(GestionPortefeuilleApplication.class, args);
    }
    

}
