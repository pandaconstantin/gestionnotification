package org.gestionportefeuille.gestionportefeuille.api;

import org.gestionportefeuille.gestionportefeuille.modele.Achat;
import org.gestionportefeuille.gestionportefeuille.modele.AchatDto;
import org.gestionportefeuille.gestionportefeuille.modele.Client;
import org.gestionportefeuille.gestionportefeuille.modele.Produit;

import java.util.List;

/**
 * Classe qui permet de recenser les API pour la gestion des clients.
 */
public interface GestionClientApiClient {
    /**
     * Créer un client.
     * @param client Un client
     * @return Client
     */
    Client createClient(Client client);

    /**
     * Met à jour un client.
     * @param client Wrapper contenant les information de modification.
     * @param idClient Identifiant du client.
     * @return Client
     */
    Client updateClient(Client client, String idClient);

    /**
     * Supprimer un client.
     * @param idClient identifiant du client.
     * @return Client.
     */
    Client deleteClient(String idClient);

    /**
     * Retourne un client en fournissant son identifiant.
     * @param idClient Identifiant du client
     * @return Client
     */
    Client getClientFromId(String idClient);

    /**
     * Retourne la liste des clients.
     * @return List<Client>
     */
    List<Client> getListClients();

    /**
     * Créer un produit.
     * @param produit Produit créé
     * @return  Produit Le produit crée
     */
    Produit createProduit(Produit produit);

    /**
     * Met à jour un produit.
     * @param produit Wrapper contenant les informations
     * @param idProduit Identifiant du produit
     * @return Produit Le produit modifié
     */
    Produit updateProduit(Produit produit, String idProduit);

    /**
     * Supprime un produit
     * @param idProduit Identifiant du produit
     * @return Produit
     */
    Produit deleteProduit(String idProduit);

    /**
     * Retourne un produit.
     * @param idProduit Identifiant du produit.
     * @return Produit
     */
    Produit getProduit(String idProduit);

    /**
     * Retourne la liste des produits.
     * @return List<Produit>
     */
    List<Produit> getListProduits();

    Achat createAchat(Achat achat);

    List<AchatDto> getListAchats();
}
