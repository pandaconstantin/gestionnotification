package org.gestionportefeuille.gestionportefeuille.impl;

import lombok.RequiredArgsConstructor;
import org.gestionportefeuille.gestionportefeuille.api.GestionClientApiClient;
import org.gestionportefeuille.gestionportefeuille.modele.Achat;
import org.gestionportefeuille.gestionportefeuille.modele.AchatDto;
import org.gestionportefeuille.gestionportefeuille.modele.Client;
import org.gestionportefeuille.gestionportefeuille.modele.Produit;
import org.gestionportefeuille.gestionportefeuille.repository.AchatRepository;
import org.gestionportefeuille.gestionportefeuille.repository.ClientRepository;
import org.gestionportefeuille.gestionportefeuille.repository.ProduitRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class GestionClientApiClientImpl implements GestionClientApiClient {
    private final ClientRepository clientRepository;
    private final ProduitRepository produitRepository;
    private final AchatRepository achatRepository;

    /**
     * Créer un client.
     *
     * @param client Un client
     * @return Client
     */
    @Override
    public Client createClient(final Client client) {
        client.setIdClient(UUID.randomUUID().toString());
        return clientRepository.save(client);
    }

    /**
     * Met à jour un client.
     *
     * @param client   Wrapper contenant les information de modification.
     * @param idClient Identifiant du client.
     * @return Client
     */
    @Override
    public Client updateClient(final Client client, final String idClient) {
        Optional<Client> clientOptional = clientRepository.findById(idClient);
        if (clientOptional.isPresent()) {
            client.setIdClient(idClient);
            return clientRepository.save(client);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Client ayant l'id " + idClient + "introuvable");
        }
    }

    /**
     * Supprimer un client.
     *
     * @param idClient identifiant du client.
     * @return Client.
     */
    @Override
    public Client deleteClient(final String idClient) {
        Optional<Client> clientOptional = clientRepository.findById(idClient);
        if (clientOptional.isPresent()) {
            clientRepository.delete(clientOptional.get());
            return clientOptional.get();
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Client ayant l'id " + idClient + "introuvable");
        }
    }

    /**
     * Retourne un client en fournissant son identifiant.
     *
     * @param idClient Identifiant du client
     * @return Client
     */
    @Override
    public Client getClientFromId(final String idClient) {
        Optional<Client> clientOptional = clientRepository.findById(idClient);
        if (clientOptional.isPresent()) {
            return clientOptional.get();
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, idClient + "introuvable");
        }
    }

    /**
     * Retourne la liste des clients.
     *
     * @return List<Client>
     */
    @Override
    public List<Client> getListClients() {
        List<Client> listeClients = new ArrayList<>();
        clientRepository.findAll().forEach(listeClients::add);
        return listeClients;
    }

    /**
     * Créer un produit.
     *
     * @param produit Produit créé
     * @return Produit Le produit crée
     */
    @Override
    public Produit createProduit(final Produit produit) {
        return produitRepository.save(produit);
    }

    /**
     * Met à jour un produit.
     *
     * @param produit   Wrapper contenant les informations
     * @param idProduit Identifiant du produit
     * @return Produit Le produit modifié
     */
    @Override
    public Produit updateProduit(final Produit produit, final String idProduit) {
        Optional<Produit> produitOptional = produitRepository.findById(idProduit);
        if (produitOptional.isPresent()) {
            produit.setIdProduit(idProduit);
            return produitRepository.save(produit);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Produit avec l'identifiant " + idProduit + "introuvable");
        }
    }

    /**
     * Supprime un produit
     *
     * @param idProduit Identifiant du produit
     * @return Produit
     */
    @Override
    public Produit deleteProduit(final String idProduit) {
        Optional<Produit> produitOptional = produitRepository.findById(idProduit);
        if (produitOptional.isPresent()) {
            produitRepository.delete(produitOptional.get());
            return produitOptional.get();
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, idProduit + "introuvable");
        }
    }

    /**
     * Retourne un produit.
     *
     * @param idProduit Identifiant du produit.
     * @return Produit
     */
    @Override
    public Produit getProduit(final String idProduit) {
        Optional<Produit> produitOptional = produitRepository.findById(idProduit);
        if (produitOptional.isPresent()) {
            return produitOptional.get();
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Produit introuvable");
        }
    }

    /**
     * Retourne la liste des produits.
     *
     * @return List<Produit>
     */
    @Override
    public List<Produit> getListProduits() {
        List<Produit> listeProduits = new ArrayList<>();
        produitRepository.findAll().forEach(listeProduits::add);
        return listeProduits;
    }

    /**
     * @param achat Achat
     * @return Achat.
     */
    @Override
    public Achat createAchat(final Achat achat) {
        achat.setIdAchat(UUID.randomUUID().toString());
        achat.setDateAchat(LocalDate.now());
        return achatRepository.save(achat);
    }

    /**
     * @return Liste des achats
     */
    @Override
    public List<AchatDto> getListAchats() {
        List<AchatDto> listeAchat = new ArrayList<>();
        achatRepository.findAll().forEach(a -> {
            AchatDto achatDto = new AchatDto();
            achatDto.setIdAchat(a.getIdAchat());
            achatDto.setIdClient(a.getIdClient());
            achatDto.setDateAchat(a.getDateAchat());
            achatDto.setNomClient(clientRepository.findById(a.getIdClient()).get().getNom());
            achatDto.setPrenomClient(clientRepository.findById(a.getIdClient()).get().getPrenom());
            achatDto.setLibelleProduit(produitRepository.findById(a.getIdProduit()).get().getLibelle());
            listeAchat.add(achatDto);
        });
        return listeAchat;
    }
}
