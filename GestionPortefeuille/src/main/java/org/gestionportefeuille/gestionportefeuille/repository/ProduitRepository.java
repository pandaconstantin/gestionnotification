package org.gestionportefeuille.gestionportefeuille.repository;

import org.gestionportefeuille.gestionportefeuille.modele.Produit;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface ProduitRepository extends ElasticsearchRepository<Produit, String> {
}
