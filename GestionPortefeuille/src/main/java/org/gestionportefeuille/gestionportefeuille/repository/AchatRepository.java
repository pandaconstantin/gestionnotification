package org.gestionportefeuille.gestionportefeuille.repository;

import org.gestionportefeuille.gestionportefeuille.modele.Achat;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;



public interface AchatRepository extends ElasticsearchRepository<Achat, String> {
}
