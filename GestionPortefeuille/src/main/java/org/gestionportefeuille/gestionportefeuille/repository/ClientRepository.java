package org.gestionportefeuille.gestionportefeuille.repository;

import org.gestionportefeuille.gestionportefeuille.modele.Client;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


public interface ClientRepository extends ElasticsearchRepository<Client, String> {
}
