package org.gestionportefeuille.gestionportefeuille.controler;

import lombok.RequiredArgsConstructor;
import org.gestionportefeuille.gestionportefeuille.modele.AchatDto;
import org.gestionportefeuille.gestionportefeuille.modele.Client;
import org.gestionportefeuille.gestionportefeuille.services.GestionClientServices;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/client-rest")
public class GestionClientRESTController {
    private final GestionClientServices services;

    @GetMapping("/client")
    public ResponseEntity<List<Client>> getListeClientEligibles() {
        return new ResponseEntity<>(services.listeClient(), HttpStatus.OK) ;
    }

    @GetMapping("/achat")
    public ResponseEntity<List<AchatDto>> getListeAchatForNotification() {
        return new ResponseEntity<>(services.listeAchats(), HttpStatus.OK);
    }
}
