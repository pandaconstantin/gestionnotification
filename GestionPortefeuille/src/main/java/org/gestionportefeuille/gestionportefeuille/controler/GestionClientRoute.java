package org.gestionportefeuille.gestionportefeuille.controler;

import lombok.RequiredArgsConstructor;
import org.gestionportefeuille.gestionportefeuille.modele.Achat;
import org.gestionportefeuille.gestionportefeuille.modele.Client;
import org.gestionportefeuille.gestionportefeuille.modele.Produit;
import org.gestionportefeuille.gestionportefeuille.services.GestionClientServices;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/route")
@Controller
@RequiredArgsConstructor
public class GestionClientRoute {
    private final GestionClientServices services;
    /**
     * Route sur la page d'enregistrement
     * @param model Objet de type Model. Wrapper qui contient les informations destinées au ViewResolver
     * @return La vue pour l'enregistrement d'un client
     */
    @GetMapping("/enregistrerclient")
    public String getPageEnregistrerClient(Model model) {
        model.addAttribute("payload", new Client());
        return "enregistrerclient";
    }

    /**
     * Route sur la page d'enregistrement des produits
     * @param model Objet de type Model.
     * @return La vue pour l'enregistrement d'un produit.
     */
    @GetMapping("/enregistrerproduit")
    public String getPageEnregistrerProduit(Model model) {
        model.addAttribute("payload", new Produit());
        return "enregistrerproduit";
    }

    /**
     * Route sur la page d'enregistrement des achats.
     * @param model Objet de type Model.
     * @return La vue pour l'enregistrement d'un achat.
     */
    @GetMapping("/enregistrerachat")
    public String getPageEnregistrerAchat(Model model) {
        model.addAttribute("payload", new Achat());
        model.addAttribute("clients", services.listeClient());
        model.addAttribute("produits", services.listeProduit());
        return "enregistrerachat";
    }

    @GetMapping("/index")
    public String getHomePage(Model model) {
        return "index";
    }
}
