package org.gestionportefeuille.gestionportefeuille.controler;

import lombok.RequiredArgsConstructor;
import org.gestionportefeuille.gestionportefeuille.modele.Achat;
import org.gestionportefeuille.gestionportefeuille.modele.Client;
import org.gestionportefeuille.gestionportefeuille.modele.Produit;
import org.gestionportefeuille.gestionportefeuille.services.GestionClientServices;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/client")
@Controller
@RequiredArgsConstructor
public class GestionClientControler {

    private final GestionClientServices services;

    @PostMapping("/portefeuille-client")
    public String enregistrerClient(@ModelAttribute("payload") Client payload) {
        services.enregistrerClient(payload);
        return "redirect:/route/enregistrerclient";
    }

    @GetMapping("/portefeuille-client")
    public String listerClients(Model model) {
        model.addAttribute("clients", services.listeClient());
        return "listerclient";
    }

    @PostMapping("/portefeuille-produit")
    public String enregistrerProduit(@ModelAttribute("payload")Produit payload) {
        services.enregistrerProduit(payload);
        return "redirect:/route/enregistrerproduit";
    }

    @GetMapping("/portefeuille-produit")
    public String listerProduit(Model model) {
        model.addAttribute("produits", services.listeProduit());
        return "listerproduit";
    }

    @PostMapping("/portefeuille-achat")
    public String enregistrerAchat(@ModelAttribute("payload") Achat payload) {
        services.enregistrerAchat(payload);
        return "redirect:/route/enregistrerachat";
    }

    @GetMapping("/portefeuille-achat")
    public String listerAchat(Model model) {
        model.addAttribute("achats", services.listeAchats());
        return "listerachats";
    }
}
