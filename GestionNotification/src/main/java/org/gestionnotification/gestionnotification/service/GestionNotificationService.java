package org.gestionnotification.gestionnotification.service;

import lombok.RequiredArgsConstructor;
import org.gestionnotification.gestionnotification.api.GestionNotificationAPIClient;
import org.gestionnotification.gestionnotification.modele.Client;
import org.gestionnotification.gestionnotification.modele.EmailNotification;
import org.gestionnotification.gestionnotification.modele.Notification;
import org.gestionnotification.gestionnotification.modele.SMSNotification;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class GestionNotificationService {

    private final GestionNotificationAPIClient gestionNotificationAPIClient;

    public SMSNotification enregistrerSMS(final SMSNotification smsNotification) {
        return gestionNotificationAPIClient.createSMSNotification(smsNotification);
    }

    public List<SMSNotification> listerSMSNotification() {
        return gestionNotificationAPIClient.getListeSMSNotification();
    }

    public EmailNotification enregistrerEmail(final EmailNotification emailNotification) {
        return gestionNotificationAPIClient.createEmailNotification(emailNotification);
    }

    public List<EmailNotification> listerEmailNotification() {
        return gestionNotificationAPIClient.getListeEmailNotification();
    }

    public List<Notification> listerNotification() {
        return gestionNotificationAPIClient.getListeNotification();
    }

    public List<Client> getListeClientsEligibles(final int nombreAchat) {
        return gestionNotificationAPIClient.getListeClientWithANumberOfAchat(nombreAchat);
    }
}
