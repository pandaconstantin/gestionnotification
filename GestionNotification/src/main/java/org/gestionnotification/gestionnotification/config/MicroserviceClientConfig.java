package org.gestionnotification.gestionnotification.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class MicroserviceClientConfig {
    @Value("${service.baseurl}")
    private String baseUrl;
    @Bean
    public WebClient WebClientServiceClient() {
        WebClient webClient = WebClient.builder().baseUrl(baseUrl).build();
        return webClient;
    }
}
