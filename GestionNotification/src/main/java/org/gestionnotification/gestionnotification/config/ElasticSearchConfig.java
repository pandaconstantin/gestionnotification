package org.gestionnotification.gestionnotification.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.client.ClientConfiguration;
import org.springframework.data.elasticsearch.client.elc.ElasticsearchConfiguration;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

@Configuration
@EnableElasticsearchRepositories(basePackages = "org.gestionnotification.gestionnotification.repository")
public class ElasticSearchConfig extends ElasticsearchConfiguration {

    @Value("${elasticsearch.server.uri}")
    private String serverAdresse;
    @Value("${elasticsearch.server.username}")
    private String username;
    @Value("${elasticsearch.server.password}")
    private String password;
    @Override
    public ClientConfiguration clientConfiguration() {

        return ClientConfiguration.builder()
                .connectedTo(serverAdresse)
                .withBasicAuth(username, password).build();
    }
}
