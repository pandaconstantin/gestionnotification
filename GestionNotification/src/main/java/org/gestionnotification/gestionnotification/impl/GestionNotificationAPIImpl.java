package org.gestionnotification.gestionnotification.impl;

import lombok.RequiredArgsConstructor;
import org.gestionnotification.gestionnotification.api.GestionNotificationAPIClient;
import org.gestionnotification.gestionnotification.config.MicroserviceClientConfig;
import org.gestionnotification.gestionnotification.modele.AchatDto;
import org.gestionnotification.gestionnotification.modele.Client;
import org.gestionnotification.gestionnotification.modele.EmailNotification;
import org.gestionnotification.gestionnotification.modele.Notification;
import org.gestionnotification.gestionnotification.modele.SMSNotification;
import org.gestionnotification.gestionnotification.repository.EmailNotificationRepository;
import org.gestionnotification.gestionnotification.repository.NotificationRepository;
import org.gestionnotification.gestionnotification.repository.SMSNotificationRepository;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class GestionNotificationAPIImpl implements GestionNotificationAPIClient {
    private final SMSNotificationRepository smsNotificationRepository;
    private final EmailNotificationRepository emailNotificationRepository;
    private final NotificationRepository notificationRepository;
    private final MicroserviceClientConfig webClientServiceClient;
    @Override
    public SMSNotification createSMSNotification(final SMSNotification smsNotification) {
        return smsNotificationRepository.save(smsNotification);
    }

    @Override
    public List<SMSNotification> getListeSMSNotification() {
        List<SMSNotification> listeSMSNotification = new ArrayList<>();
        smsNotificationRepository.findAll().forEach(listeSMSNotification::add);
        return listeSMSNotification;
    }

    @Override
    public EmailNotification createEmailNotification(final EmailNotification emailNotification) {
        return emailNotificationRepository.save(emailNotification);
    }

    @Override
    public List<EmailNotification> getListeEmailNotification() {
        List<EmailNotification> listeEmailNotification = new ArrayList<>();
        emailNotificationRepository.findAll().forEach(listeEmailNotification::add);
        return listeEmailNotification;
    }

    @Override
    public List<Notification> getListeNotification() {
        List<Notification> listeNotification = new ArrayList<>();
        notificationRepository.findAll().forEach(listeNotification::add);
        return listeNotification;
    }

    @Override
    public List<Client> getListClientEligible() {
        return webClientServiceClient.WebClientServiceClient().
                get().uri("/client-rest/client")
                .retrieve()
                .toEntityList(Client.class)
                .mapNotNull(HttpEntity::getBody)
                .block();
    }

    @Override
    public List<AchatDto> getListeAchatsClient() {
        return webClientServiceClient.WebClientServiceClient()
                .get().uri("/client-rest/achat")
                .retrieve()
                .toEntityList(AchatDto.class)
                .mapNotNull(HttpEntity::getBody)
                .block();
    }

    @Override
    public List<Client> getListeClientWithANumberOfAchat(final int numberOfAchats) {
        // Lister l'identifiant des clients qui ont effectué n achats
        List<String> listeIdClientEligible = new ArrayList<>();
        listeIdClientEligible = this.getListeAchatsClient().stream()
                .collect(Collectors.groupingBy(AchatDto::getIdClient, Collectors.counting()))
                .entrySet().stream()
                .filter(e -> e.getValue() == numberOfAchats)
                .map(t -> t.getKey())
                .collect(Collectors.toList());

        List<String> finalListeIdClientEligible = listeIdClientEligible;
        return this.getListClientEligible().stream().filter(c -> finalListeIdClientEligible
                .contains(c.getIdClient()))
                .toList();
    }
}
