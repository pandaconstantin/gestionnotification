package org.gestionnotification.gestionnotification.api;

import org.gestionnotification.gestionnotification.modele.AchatDto;
import org.gestionnotification.gestionnotification.modele.Client;
import org.gestionnotification.gestionnotification.modele.EmailNotification;
import org.gestionnotification.gestionnotification.modele.Notification;
import org.gestionnotification.gestionnotification.modele.SMSNotification;
import java.util.List;

public interface GestionNotificationAPIClient {
    SMSNotification createSMSNotification(SMSNotification smsNotification);
    List<SMSNotification> getListeSMSNotification();
    EmailNotification createEmailNotification(EmailNotification emailNotification);
    List<EmailNotification> getListeEmailNotification();
    List<Notification> getListeNotification();
    List<Client> getListClientEligible();
    List<AchatDto> getListeAchatsClient();
    List<Client> getListeClientWithANumberOfAchat(int numberOfAchats);
}
