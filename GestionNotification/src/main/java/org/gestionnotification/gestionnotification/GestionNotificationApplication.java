package org.gestionnotification.gestionnotification;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestionNotificationApplication {

    public static void main(String[] args) {
        SpringApplication.run(GestionNotificationApplication.class, args);
    }

}
