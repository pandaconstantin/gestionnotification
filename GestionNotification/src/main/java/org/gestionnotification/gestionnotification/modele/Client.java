package org.gestionnotification.gestionnotification.modele;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Client {
    private String idClient;
    private String nom;
    private String prenom;
    private String numeroTelephone;
    private String emailAdresse;
}
