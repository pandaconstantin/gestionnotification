package org.gestionnotification.gestionnotification.modele;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Document(indexName = "emailnotification")
public class EmailNotification extends Notification {
    @Field(name = "emailclient")
    private String emailClient;
}
