package org.gestionnotification.gestionnotification.modele;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Document(indexName = "notification")
public abstract class Notification {
    @Id
    private String idNotification;
    @Field(name = "idclient")
    private String idClient;
    @Field(name = "nomclient")
    private String nomClient;
    @Field(name = "prenomclient")
    private String prenomClient;
}
