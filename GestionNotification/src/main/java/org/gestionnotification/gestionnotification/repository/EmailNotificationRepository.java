package org.gestionnotification.gestionnotification.repository;

import org.gestionnotification.gestionnotification.modele.EmailNotification;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface EmailNotificationRepository extends ElasticsearchRepository<EmailNotification, String> {
}
