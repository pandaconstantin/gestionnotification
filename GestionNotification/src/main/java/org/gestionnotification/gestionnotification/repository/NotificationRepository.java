package org.gestionnotification.gestionnotification.repository;

import org.gestionnotification.gestionnotification.modele.Notification;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface NotificationRepository  extends ElasticsearchRepository<Notification, String> {
}
