package org.gestionnotification.gestionnotification.repository;

import org.gestionnotification.gestionnotification.modele.SMSNotification;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface SMSNotificationRepository extends ElasticsearchRepository<SMSNotification, String> {
}
