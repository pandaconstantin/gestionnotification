package org.gestionnotification.gestionnotification.controler;

import lombok.RequiredArgsConstructor;
import org.gestionnotification.gestionnotification.service.GestionNotificationService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequiredArgsConstructor
@RequestMapping("/notification")
@Controller
public class GestionNotificationControler {
    private final GestionNotificationService service;

    @GetMapping("/notification")
    public String getListeNotification(Model model) {
        model.addAttribute("notifications", service.listerNotification());
        return "listenotification";
    }

    @GetMapping("/client-eligible")
    public String getListeClientEligible(Model model) {
        model.addAttribute("clients", service.getListeClientsEligibles(5));
        return "listeclienteligile";
    }
}
