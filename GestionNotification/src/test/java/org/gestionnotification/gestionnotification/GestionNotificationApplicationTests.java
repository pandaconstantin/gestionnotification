package org.gestionnotification.gestionnotification;

import org.gestionnotification.gestionnotification.api.GestionNotificationAPIClient;
import org.gestionnotification.gestionnotification.modele.AchatDto;
import org.gestionnotification.gestionnotification.modele.Client;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class GestionNotificationApplicationTests {

    @Autowired  private GestionNotificationAPIClient gestionNotificationAPIClient;

    @Test
    void contextLoads() {
    }

    @Test
    public void when_client_remote_then_all_list() {
        //Arrange & Act
        List<Client> listeClients  = gestionNotificationAPIClient.getListClientEligible();
        System.out.println("Nombre de client ::: " + listeClients.size());
        //Assertion
        Assertions.assertEquals(3, listeClients.size());
    }

    @Test
    public void when_achat_remote_then_all_list() {
        List<AchatDto> listeAchats = gestionNotificationAPIClient.getListeAchatsClient();
        System.out.println("Nombre de achats ::: " + listeAchats.size());
        Assertions.assertEquals(false, listeAchats.isEmpty());
    }

    @Test
    public void when_nombreAchat_then_liste_client_eligible() {
        List<Client> listeClients = gestionNotificationAPIClient.getListeClientWithANumberOfAchat(0);
        System.out.println("Liste des clients eligibles " + listeClients.size());
        listeClients.forEach(System.out::println);

    }

}
